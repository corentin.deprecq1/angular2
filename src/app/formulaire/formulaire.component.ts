import { Component } from '@angular/core';
import { Identification } from '../class/Identification'
import {ConnectService} from "../connect.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent {

  ident: Identification = new Identification("", "")

  token: string = ""
  constructor(public service: ConnectService, public router: Router) {
  }

  connect(ident: Identification) {
    this.service.auth(ident).subscribe({
      error: (err) => this.onError(err),
      next: (data) => this.onConnect(data)
    })
  }

  private onError(err: any) {
    console.log(err)
  }

  private onConnect(data: string) {
    console.log(data)
    localStorage.setItem("token", data)
    this.token = data
    this.router.navigate(["chat"])
  }

  protected() {
    const token = localStorage.getItem("token")
    this.service.sendAuth(token === null ? "" : token).subscribe({
      error: (err) => this.onError(err),
      next: (data) => this.onConnect(data)
    })
  }
}

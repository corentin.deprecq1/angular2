export class Identification {

  constructor(
    public ident: string,
    public pass: string
  ) {
  }

  check() {
    return this.ident === null || this.ident === ""
      || this.pass === null || this.pass === "";
  }
}

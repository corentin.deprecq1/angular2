export class ChatMessage {

  public message: string = ""
  public auteur: string = ""
  public timestamp: Date = new Date()
  public action: string = ""
  public subject: string = ""

  constructor() {
  }

  public sign(text: string) {
    this.action = "MESSAGE"
    this.subject = ""
    this.message = text
    this.auteur = "Corentin"
    this.timestamp = new Date()
  }
}

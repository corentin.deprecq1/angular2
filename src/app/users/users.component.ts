import {Component, Input} from '@angular/core';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {
  @Input() list: string[] | any

  public open: boolean = true

  setOpen() {
    this.open = !this.open
  }
}

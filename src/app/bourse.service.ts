import { Injectable } from '@angular/core';
import {webSocket} from "rxjs/webSocket";
import {ChatMessage} from "./class/ChatMessage";

@Injectable({
  providedIn: 'root'
})
export class BourseService {

  public socket$: any;

  constructor() { }

  public connect() {
    console.log("Connexion au socket bourse")
    this.socket$ = webSocket("ws://monsieurjs.ddns.net/bourse")
  }
}

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {ConnectService} from "./connect.service";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private service: ConnectService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.service.getAuthStorage()
    const isApiUrl = request.url.startsWith("http://localhost:8080/auth/protected");

    if (isApiUrl) {
      request = request.clone({
        setHeaders: { Authorization: `Bearer ${token}` }
      })
    }

    return next.handle(request);
  }
}

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Identification} from "./class/Identification";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ConnectService {

  /* Url de l'API */
  private url = "http://localhost:8080"

  constructor(private http: HttpClient) {
  }

  public auth(ident: Identification): Observable<string> {
    return this.http.get(this.url + "/auth/token?subject=" + ident.ident + "&principal=" + ident.pass, {"responseType": "text"})
  }

  public sendAuth(token: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }),
      responseType: 'text' as const
    }
    return this.http.get(this.url + "/auth/protected", {responseType: "text"})
  }

  public getAuthStorage() {
    return localStorage.getItem("token")
  }
}

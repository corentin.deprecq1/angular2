import { Injectable } from '@angular/core';
import {ChatMessage} from "./class/ChatMessage";
import {webSocket} from "rxjs/webSocket";

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public socket$: any;

  constructor() { }

  public connect(user: string) {
    console.log("Connexion au socket")
    this.socket$ = webSocket("ws://monsieurjs.ddns.net/chat/v2/" + user)
  }

  public send(message: ChatMessage) {
    this.socket$?.next(message)
  }
}

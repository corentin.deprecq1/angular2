import {Component, OnInit} from '@angular/core';
import {ChatService} from "../chat.service";
import {ChatMessage} from "../class/ChatMessage";
import {filter} from "rxjs";
import {BourseService} from "../bourse.service";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  public texte: string = ""
  public listeMessage: ChatMessage[] = []
  public listUser: string[] = []
  public user: string = "Corentin"

  constructor(
    public chatService: ChatService,
    public bourseSerive: BourseService
  ) {
  }

  ngOnInit(): void {
    this.chatService.connect(this.user)
    this.recive()
    this.connectMessage()
    this.bourseSerive.connect()
    this.bourseSerive.socket$.subscribe({
      next: (text: any) => {
        console.log(text)
      },
      error: (error: string) => console.log(error),
      complete: () => console.log('Observer a reçu une notification')
    })
  }

  public  send() {
    let inputMessage = document.getElementById("inputMessage")
    if (inputMessage !== null && inputMessage !== undefined) {
      inputMessage.focus()
    }
    let message = new ChatMessage()
    message.sign(this.texte)
    this.chatService.send(message)
    this.texte = ""

  }

  private recive() {
    this.chatService.socket$?.pipe(filter((event: ChatMessage) => event.action === "MESSAGE")).subscribe({
      next: (text: ChatMessage) => {
        this.listeMessage.push(text)
      },
      error: (error: string) => console.log(error),
      complete: () => console.log('Observer a reçu une notification')
    })

    let content_chat = document.getElementById("content_chat")
    if (content_chat !== null) {
      content_chat.scrollTop = content_chat.scrollHeight
    }
  }

  private connectMessage() {
    this.chatService.socket$?.pipe(filter((event: { action: string; }) => event.action === "CONNECT" || event.action === "DISCONNECT")).subscribe({
      next: (text: ChatMessage) => {
        this.listeMessage.push(text)
        if (text.action === "CONNECT") {
          this.listUser.push(text.subject)
        } else {
          const index = this.listUser.indexOf(text.subject)
          if (index > -1) {
            // delete this.listUser[index]
            this.listUser.slice(index, 1)
          }
        }
      },
      error: (error: string) => console.log(error),
      complete: () => console.log('Observer a reçu une notification')
    })
  }
}

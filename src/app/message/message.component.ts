import {Component, Input} from '@angular/core';
import {ChatMessage} from "../class/ChatMessage";

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent {

  @Input() message: ChatMessage | any
}

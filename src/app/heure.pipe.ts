import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'heure'
})
export class HeurePipe implements PipeTransform {

  transform(value: Date): string {

    const date = new Date(value)
    return (date.getDate() <= 9 ? "0" + date.getDate() : date.getDate()) + "/" + ((date.getMonth() + 1) <= 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1) + "/" + date.getFullYear() + " "
      + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
  }

}
